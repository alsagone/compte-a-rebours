/* 
    TO DO : 
    - récupérer les dates des input 
    - fonction qui vérifie si une date est valide et affiche 
    un mesage d'erreur
    - trier deux dates et les renvoyer dans un tableau trié 
    de telle sorte que dans la fonction principale 
    date1 = dateTriees[0] et date2 = datesTriees[1] avec date1 <= date2

    
    */
function estCompris(x, a, b) {
    var myMin = Math.min(a, b);
    var myMax = Math.max(a, b);

    return (myMin <= x) && (x <= myMax);
}

function nombreJoursMois(mois, annee) {

    var n;
    if (mois == 2) {
        n = estBissextile(annee) ? 29 : 28;
    } else {

        // Mois à 30 jours = avril - juin - septembre - novembre 
        switch (mois) {
            case 4:
                n = 30;
                break;
            case 6:
                n = 30;
                break;
            case 9:
                n = 30;
                break;
            case 11:
                n = 30;
                break;
            default:
                n = 31;
                break;
        }
    }

    return n;
}

function newDate(jour, mois, annee) {
    return { jour: jour, mois: mois, annee: annee };
}

function estUneDateValide(d) {
    nbJoursMois = nombreJoursMois(d.mois, d.annee);
    return (estCompris(d.jour, 1, nbJoursMois) && (estCompris(d.mois, 1, 12)));
}

function trierDates(date1, date2) {
    var tri;
    if (date1.annee < date2.annee) {
        tri = [date1, date2];
    } else if ((date1.annee == date2.annee) && (date1.mois < date2.mois)) {
        tri = [date1, date2];
    } else if ((date1.annee == date2.annee) && (date1.mois == date2.mois) && (date1.jour < date2.jour)) {
        tri = [date1, date2];
    } else {
        tri = [date2, date1];
    }

    return tri;
}


function getDates() {
    var jour1 = parseInt(document.getElementById("jour1").value, 10);
    var mois1 = parseInt(document.getElementById("mois1").value, 10);
    var annee1 = parseInt(document.getElementById("annee1").value, 10);

    var jour2 = parseInt(document.getElementById("jour2").value, 10);
    var mois2 = parseInt(document.getElementById("mois2").value, 10);
    var annee2 = parseInt(document.getElementById("annee2").value, 10);

    var date1 = newDate(jour1, mois1, annee1);
    var date2 = newDate(jour2, mois2, annee2);

    return trierDates(date1, date2);
}

function format2Chiffres(n) {
    if (n < 10) {
        format = "0" + n;
    } else {
        format = "" + n;
    }

    return format;
}

function dateToString(d) {
    var jour = format2Chiffres(d.jour);
    var mois = format2Chiffres(d.mois);
    var annee = d.annee;
    return jour + "/" + mois + "/" + annee;
}

function datesEgales(date1, date2) {
    return ((date1.jour == date2.jour) && (date1.mois == date2.mois) && (date1.annee == date2.annee))
}

function estBissextile(annee) {
    var b;

    if ((annee % 4 == 0) && (annee % 100 != 0)) {
        b = true;
    } else if (annee % 400 == 0) {
        b = true;
    } else {
        b = false;
    }

    return b;
}

function ajouterJour(d) {

    var jour = d.jour;
    var mois = d.mois;
    var annee = d.annee;


    if ((jour == 31) && (mois == 12)) {
        jour = 1;
        mois = 1;
        annee++;
    } else if (jour == nombreJoursMois(mois, annee)) {
        jour = 1;
        mois++;
    } else {
        jour++;
    }

    return newDate(jour, mois, annee);

}

function nombreJours(date1, date2) {
    var tri = trierDates(date1, date2);
    var depart = tri[0];
    var arrivee = tri[1];
    var nbJours = 0;

    while (!datesEgales(depart, arrivee)) {

        depart = ajouterJour(depart);
        nbJours++;
    }

    return nbJours;
}

function test() {
    var date1 = getDate1();
    var date2 = getDate2();
    console.log("date1 = " + dateToString(date1));
    console.log("date2 = " + dateToString(date2));
    console.log("nbJours = " + nombreJours(date1, date2));
}

function ajouterResultat(message) {
    var ul = document.getElementById("listeResultats");
    var li = document.createElement("li");
    li.appendChild(document.createTextNode(message));

    if (message.includes("Erreur")) {
        li.style.color = "red";
    }
    ul.appendChild(li);
    return;
}

function action() {
    var dates = getDates();
    var date1 = dates[0];
    var date2 = dates[1];

    var nbJours, message;

    if (!estUneDateValide(date1)) {
        message = "Erreur sur la date 1 : " + dateToString(date1) + " n\'est pas une date valide.";
    } else if (!estUneDateValide(date2)) {
        message = "Erreur sur la date 2 : " + dateToString(date2) + " n\'est pas une date valide.";
    } else {
        nbJours = nombreJours(date1, date2);
        message = nbJours + " jours entre le " + dateToString(date1) + " et le " + dateToString(date2) + ".";

        if (nbJours < 2) {
            message = message.replace("jours", "jour");
        }
    }

    ajouterResultat(message);
    return;
}

function remplirDate(jour, mois, annee, idInput) {
    if (idInput == "date1") {
        document.getElementById("jour1").value = jour;
        document.getElementById("mois1").value = mois;
        document.getElementById("annee1").value = annee;
    } else {
        document.getElementById("jour2").value = jour;
        document.getElementById("mois2").value = mois;
        document.getElementById("annee2").value = annee;
    }

    return;
}

function viderDate(idInput) {
    if (idInput == "date1") {
        document.getElementById("jour1").value = "";
        document.getElementById("mois1").value = "";
        document.getElementById("annee1").value = "";
    } else {
        document.getElementById("jour2").value = "";
        document.getElementById("mois2").value = "";
        document.getElementById("annee2").value = "";
    }

    return;
}


document.addEventListener('DOMContentLoaded', (event) => {
    var checkbox1 = document.getElementById("aujourdHui1");
    var checkbox2 = document.getElementById("aujourdHui2");
    console.log(checkbox1);
    console.log(checkbox2);
    checkbox1.addEventListener('change', function() {
        if (this.checked) {
            var aujourdHui = new Date();
            var jour = aujourdHui.getDate();
            var mois = aujourdHui.getMonth() + 1;
            var annee = aujourdHui.getFullYear();
            console.log(jour + "/" + mois + "/" + annee);
            remplirDate(jour, mois, annee, "date1");
        } else {
            viderDate("date1");
        }
    });

    checkbox2.addEventListener('change', function() {
        if (this.checked) {
            var aujourdHui = new Date();
            var jour = aujourdHui.getDate();
            var mois = aujourdHui.getMonth() + 1;
            var annee = aujourdHui.getFullYear();
            console.log(jour + "/" + mois + "/" + annee);
            remplirDate(jour, mois, annee, "date2");

        } else {
            viderDate("date2");
        }
    });
    //the event occurred
})